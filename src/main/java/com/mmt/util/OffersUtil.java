package com.mmt.util;

import java.util.ArrayList;
import java.util.List;

import com.mmt.model.Offer;
import com.mmt.model.OfferDTO;
import com.mmt.model.OffersDTO;

public class OffersUtil {

	public static OfferDTO convertToOfferDTO(Offer offer) {
		
		OfferDTO dto = new OfferDTO();
		dto.setDiscountValue(offer.getDiscountValue());
		dto.setId(offer.getId());
		dto.setOfferDesc(offer.getOfferDesc());
		dto.setOfferName(offer.getOfferName());
		dto.setOfferType(offer.getOfferType());
		dto.setOfferTag(offer.getOfferTag());
		dto.setDiscountType(offer.getDiscountType());
		
		return dto;
		
	}
	
	public static OffersDTO convertToOfferDTO(List<Offer> offers) {
		
		OffersDTO dto = new OffersDTO();
		List<OfferDTO> list = new ArrayList<OfferDTO>();
		dto.setDtoList(list);
		
		for (Offer offer: offers) {
			list.add(convertToOfferDTO(offer));
		}
		return dto;
		
	}
	
}
