package com.mmt.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.maps.model.LatLng;
import com.mmt.model.GetOfferRequestDTO;
import com.mmt.model.Offer;
import com.mmt.model.OfferTag;

public class OfferDAOImpl implements OfferDAO {
	private final static Logger LOGGER = Logger.getLogger(OfferDAOImpl.class);
	@Autowired
	private SessionFactory sessionFactory;
	
	public void saveOffer(Offer offer) {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Transaction tx = session.beginTransaction();
			session.saveOrUpdate(offer);
			tx.commit();
		} catch (Exception e) {
			LOGGER.error("saveOffer - exception occured - ", e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	public void saveOffers(List<Offer> offers) {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Transaction tx = session.beginTransaction();
			for (Offer offer : offers) {
				session.saveOrUpdate(offer);
			}
			tx.commit();
		} catch (Exception e) {
			LOGGER.error("saveOffer - exception occured - ", e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	public List<Offer> getOffersByLocation(GetOfferRequestDTO getOfferReqDTO) {

		List<Offer> offers = null;
		Session session = null;
		try {
			OfferTag tag = getOfferReqDTO.getOfferTag();
			LatLng latLng = getOfferReqDTO.getLatLng();
			double radius = getOfferReqDTO.getRadius();
			
			double lat = latLng.lat;
			double lng = latLng.lng;
			
			double lowLat = lat - radius;
			double hiLat = lat + radius;
			
			double lowLng = lng - radius;
			double hiLng = lng + radius;
			
			session = sessionFactory.openSession();
			Criteria criteria = session.createCriteria(Offer.class);
			criteria.add(Restrictions.eq("offerTag", tag).ignoreCase());
			criteria.add(Restrictions.between("lat", lowLat, hiLat));
			criteria.add(Restrictions.between("lng", lowLng, hiLng));
			
			offers = criteria.list();
			session.flush();
			session.close();
			
		} catch (Exception e) {
			LOGGER.error("exception occured - ", e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return offers;
	
	}
}
