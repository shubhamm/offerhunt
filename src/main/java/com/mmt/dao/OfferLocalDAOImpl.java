package com.mmt.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mmt.model.DiscountType;
import com.mmt.model.GetOfferRequestDTO;
import com.mmt.model.Offer;
import com.mmt.model.OfferTag;
import com.mmt.model.OfferType;

public class OfferLocalDAOImpl implements OfferDAO {

	private static Map<String, List<Offer>> hotelIdToOffersMap;
	
	static {
		hotelIdToOffersMap = new HashMap<String, List<Offer>>();
		List<Offer> offers = new ArrayList<Offer>();
		Offer offer1 = buildOffer(1, "MMTSALE" , "Get Cashback worth 500/- in wallet.", OfferType.CASHBACK_TO_WALLET, 500, OfferTag.RESTAURANTS, DiscountType.NET_DISCOUNT);
		Offer offer2 = buildOffer(1, "ALIAATMMT" , "Get dinner date with Alia worth 1000/-. ", OfferType.CASHBACK_TO_WALLET, 1000, OfferTag.RESTAURANTS, DiscountType.NET_DISCOUNT);
		offers.add(offer1);
		offers.add(offer2);
		hotelIdToOffersMap.put("HongKonghtl1", offers);
	}
	
	
	public void saveOffer(Offer offer) {
		
	}

	private static Offer buildOffer(int i, String name, String desc, OfferType offerType, int discValue,
			OfferTag tag, DiscountType discType) {
		
		Offer offer = new Offer();
		offer.setId(i);
		offer.setDiscountType(discType);
		offer.setDiscountValue(discValue);
		offer.setOfferDesc(desc);
		offer.setOfferName(name);
		offer.setOfferTag(tag);
		offer.setOfferType(offerType);
		return offer;
	}

	public void saveOffers(List<Offer> offers) {
		
	}

	public List<Offer> getOffersByLocation(GetOfferRequestDTO requestDTO) {
		return hotelIdToOffersMap.get("HongKonghtl1");
	}

}
