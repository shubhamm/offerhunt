package com.mmt.dao;

import java.util.List;

import com.mmt.model.GetOfferRequestDTO;
import com.mmt.model.Offer;

public interface OfferDAO {

	void saveOffer(Offer offer);
	
	void saveOffers(List<Offer> offers);
	
	List<Offer> getOffersByLocation(GetOfferRequestDTO requestDTO);
	
}
