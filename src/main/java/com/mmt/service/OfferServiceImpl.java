package com.mmt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mmt.dao.OfferDAO;
import com.mmt.model.GetOfferRequestDTO;
import com.mmt.model.Offer;

@Service
public class OfferServiceImpl implements OfferService {

	@Autowired
	private OfferDAO dao;
	

	public List<Offer> getOffersByUserLocation(GetOfferRequestDTO requestDTO) {
		List<Offer> offers = dao.getOffersByLocation(requestDTO);
		return offers;
	}

	public void saveOffers(List<Offer> offers) {
		// TODO Auto-generated method stub
		
	}

	public void saveOffer(Offer offer) {
		// TODO Auto-generated method stub
		
	}
	
	
	


	

}
