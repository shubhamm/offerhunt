package com.mmt.service;

import java.util.List;

import com.google.maps.model.LatLng;
import com.mmt.model.GetOfferRequestDTO;
import com.mmt.model.Offer;
import com.mmt.model.OfferTag;

public interface OfferService {

	/*
	 * return list of offers by x y co-ordinates
	 */
	public List<Offer> getOffersByUserLocation(GetOfferRequestDTO getOfferReqDTO);
	public void saveOffers(List<Offer> offers);
	public void saveOffer(Offer offer);

}
