package com.mmt.model;

import com.google.maps.model.LatLng;

public class GetOfferRequestDTO {

	private String email;
	private String deviceId;
	private LatLng latLng;
	private OfferTag offerTag;
	private double radius;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public LatLng getLatLng() {
		return latLng;
	}
	public void setLatLng(LatLng latLng) {
		this.latLng = latLng;
	}

	public void setOfferTag(OfferTag offerTag) {
		this.offerTag = offerTag;
	}
	
	public OfferTag getOfferTag() {
		return offerTag;
	}
	
	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public double getRadius() {
		return radius;
	}
}
