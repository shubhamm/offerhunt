package com.mmt.model;

public enum OfferType {

	CASHBACK_TO_WALLET, CASHBACK_TO_CARD;
	
}
