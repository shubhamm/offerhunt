package com.mmt.model;

import java.util.List;

public class OffersDTO {

	private List<OfferDTO> dtoList;
	private String resultCode;
	private String resultMsg;
	
	public void setDtoList(List<OfferDTO> dtoList) {
		this.dtoList = dtoList;
	}
	
	public List<OfferDTO> getDtoList() {
		return dtoList;
	}
	
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	
	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
	
	public String getResultCode() {
		return resultCode;
	}
	
	public String getResultMsg() {
		return resultMsg;
	}
	
}
