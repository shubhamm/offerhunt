package com.mmt.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.maps.model.LatLng;
import com.mmt.model.GetOfferRequestDTO;
import com.mmt.model.OfferTag;
import com.mmt.model.OffersDTO;
import com.mmt.service.OfferService;


@Controller
@RequestMapping("/offers")
public class OffersController {

	private final static Logger LOGGER = Logger.getLogger(OffersController.class);
	
	@Autowired
	private OfferService offerService;

	
	/*
	 * - input email, deviceid, range, x-y coordinates
	 *  return offers based on input 
	 */
	@RequestMapping(value = "/getOffers", method = RequestMethod.POST)
	public OffersDTO getOffersByUserCurrentLocation(ModelMap model, HttpServletRequest request,
			HttpServletResponse response, @RequestBody GetOfferRequestDTO getOfferReqDTO) {
		
		OffersDTO dto = new OffersDTO();
		offerService.getOffersByUserLocation(getOfferReqDTO);
		return dto;
		
	}
	
	@RequestMapping(value = "/saveOffers", method = RequestMethod.POST)
	public void saveOffers(ModelMap model, HttpServletRequest request,
			HttpServletResponse response, @RequestBody String data) {
		ObjectMapper mapper = new ObjectMapper();
		OffersDTO dto = null;
		try {
			dto = mapper.readValue(data, OffersDTO.class);
		} catch (IOException e) {
		}		
	}
	

	
	
}
